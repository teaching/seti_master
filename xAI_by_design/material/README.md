# Template for presentations in LaTeX

This repository is a template for presentations made in
beamer.

## Usage

Write the actual presentation in `presentation.tex`, then:

### Easy mode
`make clean && make`

### Realistic mode
Slides are to be compiled with lualatex; no guarantees of
what happens if you do otherwise.

The Makefile make use of latexmk and its option lualatex.
My `latexrc` is the following:
```
$recorder = 1;
$pdf_mode = 1;
$bibtex_use = 2;
$lualatex = 'lualatex -synctex=1 --shell-escape --interaction=nonstopmode';
$pdflatex = 'lualatex -synctex=1 --shell-escape --interaction=nonstopmode';
```
(yes, I force it to use lualatex even when pdflatex is on)

## Notes
* `.png` images are included in the `imgs` folder if needed.
* the `laiser.bib` bibliography file needs to be manually synched
  with the one in LAISER repository
