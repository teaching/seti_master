#!/usr/bin/bash

MD5=b5372ab797e5de0fc6427ab248d3a3de
ZENODO_URL=https://zenodo.org/records/10575322/files/cabrnet_cub200.zip
CABRNET_FILE=cabrnet_cub200.zip

# Create a dedicated virtualenv and download the required packages
virtualenv -p /usr/bin/python3 ./venv/setixaitp
source ./venv/setixaitp/bin/activate
python3 -m pip install -r requirements.txt
python3 -m ipykernel install --user --name=setixaitp

# Download the cabrnet pretrained models, check the file integrity
wget $ZENODO_URL -O $CABRNET_FILE
if [[ "$(md5sum $CABRNET_FILE | awk '{print $1}')" = "$MD5" ]]
then
    echo "Files correctly downloaded"
    unzip $CABRNET_FILE -d models/cabrnet_seti
else
    echo "Mismatching md5sums for files, please retry the download"
    exit -1
fi
# Unzip the dataset file
unzip data/cub_test_tiny.zip -d data/
