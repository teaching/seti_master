# Interpretable AI: post-hoc methods and interpretable-by-design approaches

This repository contains the code for a hands-on tutorial on interpretable
artificial intelligence, for the SETI Master.

# Setup

To run this session properly, several dependencies and pretrained models must be
installed. You will need a `python3` interpreter (v3.10 at minimum), `git` and
`wget`.

For convenience, a `setup.sh` shell script is provided.
This script will setup a python virtual environment and
download several pretrained models and configuration files.

The script `launch.sh` will activate the previously created environment and
launch the jupyter notebook. Note that you need to select the proper jupyter
kernel in the web interface
