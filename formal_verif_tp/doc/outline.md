Organisation pour la préparation du tuto PFIA

## Design problème cible
contraintes:
* réseau facile à entraîner
* propriétés faciles à vérifier MAIS
  différences significatives entre outils

tâches:
* concevoir le problème:
  * entrées et sorties du modèle
  * architecture du réseau
  * propriétés à vérifier
  * vulnérabilités possibles
## Documents supports
contraintes:
* public IA mais non méthodes formelles: introduire les
  enjeux et les outils ciblés sans déborder
* alterner correctement phases théoriques et phases
  de tuto, compliqué à distance
* outil de visio qui permette le partage d'écran / les
  diapos / le code

tâches:
* plan du tutoriel (déjà quasiment fait)
* rédiger diapos
* faire une répèt'
## Artefacts de code
contraintes:
* accessible à tous les participants depuis l'extérieur
* _all in one_: dépendances inclues (un conteneur?)
* pas ou peu de contraintes matérielles (google collab)

tâches:
* liste des dépendances: ISAIEH, un solveur SMT, PyTorch, NumPy,
  un outil d'inteprétation abstraite / analyse symbolique (demander à Zak)
* décider des solveurs à utiliser (au minimum: Z3)
* générer données et entraîner réseau pour en tirer un ONNX
* avoir ISAIEH suffisamment abouti pour avoir plusieurs formats de sortie
* rédiger le notebook support du tutoriel
* mettre en place un dépôt git{hub/lab} pour stocker le notebook et faciliter
  l'installation des dépendances (Si le dernier: INRIA? CEA?)?
* rédiger des _helper scripts_ pour automatiser et cadrer les processus
  (export ONNX du modèle -> concaténation avec une propriété de sûreté
  -> lancement d'un solveur -> collecte du résultat, visualisation et
  compilation de stats)

## Agenda

* Conception du problème
  * premier brouillon de proposition vendredi 05/06
  * en fonction des résultats de test, retour mercredi 10/06
* Artefacts de code
  * tester installation OCaml sur google collab 12/06
    * (si ça ne marche pas, choix d'une méthode alternative
      de partage)
  * générations de données et réseau entraîné 16/06
  * notebook: conversion d'un modèle ONNX à une formule SMT 17/06
  * notebook: lancer un solveur sur une formule SMT 19/06
  * notebook: raffinement (choix de plusieurs solveurs,
  changer la propriété, etc.)
* Documents support
  * structure générale des diapos 10/06
  * partie "description du problème" 12/06
  * partie "introduction aux méthodes formelles" 19/06

