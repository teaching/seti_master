\frametitle{Encoding neural networks}
A key point: overapproximating non-linear behaviours with convex hulls
	is sometimes necessary, which requires keeping track of bounds of
	\emph{every} variable.


	\begin{minipage}{0.45\textwidth}
		ReLU overapproximation:
		$\hat{z} \geq 0, \hat{z} \geq z, -uz+(u-l)\hat{z} \leq
			-ul$

	\end{minipage}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{imgs/ehlers.png}
		overapproximation by Ehlers, 2017
	\end{minipage}

	Using symbolic propagation\footnote{Wang et al.,
		\emph{Formal Security Analysis of Neural Networks using Symbolic Intervals}}
	and calling to LP solvers to tighten bounds (but costly)

