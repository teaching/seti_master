from sound_pyrat.analysis_results import SingleAnalysisResults
from sound_pyrat.domains.box import Box
from sound_pyrat.partitioning.scorers.abstract_scorer import AbstractScorer
import numpy as np


class AffineFormScorer(AbstractScorer):
    def __init__(self):
        super().__init__()

    @staticmethod
    def score(itv: Box, res: SingleAnalysisResults, preferred_axis=0):
        assert "zonotopes" in res.domains.keys(), "AffineFormScorer can't be used if zonotopes are not " \
                                                  "used in the analysis"
        last_domain = res.domains["zonotopes"][-1]
        choice_factor = 100
        if preferred_axis < 0:
            preferred_axis = 0
        # splits on the dimension where the affine form on preferredAxis is the biggest wrt to the input interval
        proj = last_domain.on_axis_affine(itv, preferred_axis)

        r2Size = np.add((itv.upper - itv.lower), choice_factor * (proj.upper - proj.lower))
        scores = np.argsort(r2Size)[::-1]
        return scores